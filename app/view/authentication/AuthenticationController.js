Ext.define('SimpleCMS.view.authentication.AuthenticationController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.authentication',

    //TODO: implement central Facebook OATH handling here

    onLoginButton: function() {
        var me = this,
            view = me.getView(),
            f = view.getForm(),
            src = view.down('image').getSrc();
        if (f.isValid()) {
            f.submit({
                //jsonSubmit:true,
                params: {
                    key: MD5.get(src)
                },
                url: URI.get('api/TokenAuth', 'Authenticate', true),
                waitMsg: I18N.LoginSubmitWaitMsg,
                waitTitle: I18N.LoginSubmitWaitTitle,
                success: function(form, action) {
                    var rememberMe = form.getValues().rememberClient || false,
                        obj = Ext.decode(action.response.responseText, true),
                        tokenExpireDate,
                        msg = '未知错误';
                    if (obj.success) {
                        tokenExpireDate = rememberMe ? (new Date(new Date().getTime() + 1000 * obj.result.expireInSeconds)) : null
                        HEADERS.setCookies(HEADERS.authTokenCookieName, obj.result.accessToken, tokenExpireDate);
                        HEADERS.setCookies(HEADERS.encrptedAuthTokenName, obj.result.encryptedAccessToken, tokenExpireDate, LOCALPATH);
                        window.location.reload();
                    } else {
                        if (result.error && result.error.message)
                            msg = result.error.message + (result.error.details ? result.error.details : '');
                        TOAST.toast(
                            msg,
                            form.owner.el,
                            'bl'
                        );
                    }
                },
                failure: function(form, action) {
                    this.onRefrestVcode();
                    FAILED.form(form, action);
                },
                scope: me
            });
        }
    },

    onResetClick: function() {
        var me = this,
            view = me.getView(),
            f = view.getForm();
        if (f.isValid()) {
            f.submit({
                url: URI.get('account', 'passwordreset'),
                waitMsg: I18N.SaveWaitMsg,
                waitTitle: I18N.PasswordResetTitle,
                success: function(form, action) {
                    TOAST.show(I18N.PasswordResetSuccess, view.el, null, function() {
                        window.location.reload();
                    });
                },
                failure: FAILED.form,
                scope: me
            });
        }
    },

    onReturnClick: function() {
        window.history.back();
    },

    verifyCodeUrl: URI.get('VerifyCode', 'Get'),
    onRefrestVcode: function() {
        var me = this,
            view = me.getView(),
            img = view.down('image');
        Ext.Ajax.request({
            url: me.verifyCodeUrl,
            scope: me,
            success: function(response, opts) {
                var obj = Ext.decode(response.responseText),
                    view = this.getView();
                if (view && obj.success && obj.result && obj.result.image) {
                    view.down('image').setSrc(obj.result.image);
                }
            },
            failure: function(response, opts) {
                TOAST.toast('获取验证码失败', this.getView().el, 'bl');
            }
        })
    },

    onLoginViewShow: function() {
        this.onRefrestVcode();
    }

});