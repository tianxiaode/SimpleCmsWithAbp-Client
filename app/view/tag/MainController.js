﻿Ext.define('SimpleCMS.view.tag.MainController',{
    extend: 'SimpleCMS.ux.app.BaseViewController',
    alias: 'controller.tag',

    onTagsStoreLoad: function (store, records, successful, operation, eOpts) {
        this.getViewModel().set('count', store.getTotalCount());
    },

    onRefresh: function() {
        this.getStore('tags').load();
    },


    tagDeleteUrl: URI.get('tag', 'destroy'),
    onTagDelete: function() {
        var me = this,
            view = me.getView();
        me.onDelete(me.lookupReference('TagGrid').getSelection(),
            me.tagDeleteUrl,
            "name",
            I18N.Tag,
            function (response, opts) {
                var me = this,
                    obj = Ext.decode(response.responseText);
                Ext.Msg.hide();
                if (obj.success) {
                    me.getStore('tags').load();
                    me.lookupReference('TagGrid').getSelectionModel().deselectAll();
                }
                TOAST.show(obj.msg, view.el, 'b');

            });
    },

    tagAddUrl: URI.get('tag', 'create'),
    onTagAdd: function () {
        var me = this,
            value = me.getView().getViewModel().get('tagName');
        me.send({
            url: me.tagAddUrl,
            method:'POST',            
            jsonData: { name: value},
            success: function (response, opts) {
                var me = this,
                    view = me.getView(),
                    store = me.getStore('tags'),
                    field = me.lookupReference('tagNameField'),
                    obj = Ext.decode(response.responseText);
                Ext.Msg.hide();
                console.log(obj)
                if (obj.success) {
                    TOAST.toast(I18N.SavedAndNothing, view.el, 'b');
                    store.load();
                    field.setValue('');
                } 
            },
            failure: function (response, opts) {
                var me = this,
                    view = me.getView(),
                    field = me.lookupReference('tagNameField'),
                    obj = Ext.decode(response.responseText);
                Ext.Msg.hide();
                if (obj.error && obj.error.validationErrors) {
                    field.markInvalid(obj.error.validationErrors[0].message);
                } else {
                    FAILED.ajax(response, opts);
                }
            }
        
        }, I18N.SaveWaitMsg);

    }

});