﻿Ext.define('SimpleCMS.ux.form.field.combobox.Category', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'categorySelect',

    requires: [
        'SimpleCMS.model.Category'
    ],

    minChars: 2,
    queryMode: 'remote',
    displayField: 'displayText',
    valueField: 'value',

    store: {
        //model: 'SimpleCMS.model.Category',
        pageSize: 50,
        fields:['displayText', 'value'],
        proxy: {
            type: 'format',
            url: URI.get('category', 'getselect')
        }

    }

})
