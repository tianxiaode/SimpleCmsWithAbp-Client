﻿Ext.define('SimpleCMS.model.Tag', {
    extend: 'SimpleCMS.model.Base',

    fields: [
        { name: 'name', type: 'string', defaultValue: '' }
    ],
    idProperty: 'name'
});
