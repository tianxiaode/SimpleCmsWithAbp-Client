﻿Ext.define('SimpleCMS.model.Base', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.identifier.Negative',
        'SimpleCMS.locale.Locale'
    ],

    fields: [
        { name: 'id', type: 'int' }
    ],
    idProperty: 'id',

    identifier: 'negative',
    schema: {
        namespace: 'SimpleCMS.model'
    }
});
