﻿Ext.define("SimpleCMS.model.Category", {
    extend: "SimpleCMS.model.Base",

    fields: [
        { name: 'parentId', type: 'auto', defaultValue: null },
        { name: 'parentTitle', defaultValue: '' },
        { name: 'title', defaultValue: '' },
        { name: 'image', defaultValue: '' },
        { name: 'content', defaultValue: '' },
        { name: 'sortOrder', type: 'int', defaultValue: 0 },
        { name: 'creationTime', type: 'date', dateFormat: I18N.DatetimeIsoFormat }
    ]
});
