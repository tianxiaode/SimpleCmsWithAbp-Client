﻿Ext.define('SimpleCMS.model.Media', {
    extend: 'SimpleCMS.model.Base',

    fields: [
        { name: 'filename', defaultValue: '' },
        { name: 'description', defaultValue: '' },
        { name: 'path', defaultValue: '' },
        { name: 'type', type: 'int', defaultValue: '1', min: 1, max: 3 },
        { name: 'size', type: 'int', defaultValue: '0' },
        { name: 'creationTime', type: 'date', dateFormat: I18N.DatetimeIsoFormat }
    ]

})