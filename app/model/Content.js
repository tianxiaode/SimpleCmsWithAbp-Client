﻿Ext.define('SimpleCMS.model.Content', {
    extend: 'SimpleCMS.model.Base',

    fields: [
        { name: 'categoryId', type: 'auto', defaultValue: null },
        { name: 'categoryTitle', type: 'string', defaultValue: '' },
        { name: 'title', type: 'string', defaultValue: '' },
        { name: 'image', type: 'string', defaultValue: '' },
        { name: 'summary', type: 'string', defaultValue: '' },
        { name: 'body', type: 'string', defaultValue: '' },
        { name: 'tags', type: 'string', defaultValue: '' },
        { name: 'creationTime', type: 'date', dateFormat: I18N.DatetimeIsoFormat},
        { name: 'hits', type: 'int' },
        { name: 'sortOrder', type: 'int', defaultValue: 0 }
    ]
});
